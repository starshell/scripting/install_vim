#!/bin/bash

source "../posix_common/posix_common.sh"

if ! _ask_yes_no "Install ROS melodic"; then
    echo ''
    exit 0
fi

# Setup
_info "Setting up for install..."

if _ask_yes_no "Add ROS ubuntu package repository"; then
    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
fi


sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116

# Package Management
_info "Installing..."
sudo apt-get -y update
sudo apt-get -y install ros-melodic-desktop-full

# rosdep
_info "Gathering dependencies"
sudo rosdep init
rosdep update

# Shell setup
_info "Setting up shell paths for /opt/ros/melodic"
if _ask_yes_no "Setup bash environment"; then
    echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
fi

if _ask_yes_no "Setup zsh environment"; then
    echo "source /opt/ros/melodic/setup.zsh" >> ~/.zshrc
fi

# Install build dependencies
sudo apt-get -y install python-rosinstall python-rosinstall-generator python-wstool build-essential pip
pip install --user -U catkin_tools trollius

# Setup catkin worksapce
_info "Setting up catkin workspace"
mkdir -p $HOME/catkin_ws/src && cd $HOME/catkin_ws
catkin build

_info "Setting up shell paths for ~/catkin_ws"
if _ask_yes_no "Setup bash environment"; then
    echo "source $HOME/catkin_ws/devel/setup.bash" >> ~/.bashrc
fi

if _ask_yes_no "Setup zsh environment"; then
    echo "source $HOME/catkin_ws/devel/setup.zsh" >> ~/.zshrc
fi

_info "ROS successfully installed"
_info "Source your shell .rc"
exit 0
